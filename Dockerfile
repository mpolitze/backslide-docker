FROM node:lts-alpine

ARG BACKSLIDE_VERSION=latest

LABEL maintainer="Marius Politze <m@politze.net>"

# https://docs.npmjs.com/misc/config#unsafe-perm
# https://github.com/nodejs/node-gyp/issues/454
RUN npm install --unsafe-perm --allow-root -g backslide@$BACKSLIDE_VERSION

WORKDIR /src

ENTRYPOINT [ "bs" ]
CMD [ "--help" ]
